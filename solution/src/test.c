#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>

#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void heap_init_test(){
    void* heap = heap_init(10000);
    if(heap == NULL) {
        err("Heap Init Error\n");
    }else {
        debug_heap(stderr, heap);
    }
    debug("\n%s\n", "----- Start memory allocation -----");
    void* first_block = _malloc(100);
    void* second_block = _malloc(200);
    _malloc(100);
    void* third_block = _malloc(1);
    _malloc(1000);
    debug_heap(stderr, heap);

    debug("\n%s\n", "----- Freeing one block -----");
    _free(third_block);
    debug_heap(stderr, heap);
    printf("\n");
    debug("%s\n", "----- Freeing two blocks from several allocated ----- ");
    _free(second_block);
    _free(first_block);
    debug_heap(stderr, heap);

    debug("\n%s\n", "----- New region extends the old -----");
    _malloc(200);
    _malloc(10000);
    _malloc(11000);
    _malloc(1000);
    debug_heap(stderr, heap);
    printf("\n%s\n", "----- End of test -----");
}
